(function(){
  'use strict';

  angular
    .module('test')
    .controller('ChapterCtrl', ChapterCtrl);

  ChapterCtrl.$inject = ['$scope', '$localstorage', 'languageDictionary', '$ionicSlideBoxDelegate', 'loading', '$ionicScrollDelegate', 'chapterFactory', '$ionicModal'];

  function ChapterCtrl($scope, $localstorage, languageDictionary, $ionicSlideBoxDelegate, loading, $ionicScrollDelegate, chapterFactory, $ionicModal) {
    var vm = this;
    vm.setZoom = setZoom;
    vm.pages = [];
    vm.openModal = openModal;
    var userHasBeenZoom = false;

    activate();

    function activate(){
      setCurrentLanguage();
      loading.showLoading();
      setTutorialImages();
      showModal();
      // openModal();
      //showAdmobInterstital();
      getChapter();
      getChapterId();
      setLastChapterRead();
    }

    function getScrollHandle(page){
      return $ionicScrollDelegate.$getByHandle('scroll-page-' + page);
    }

    function getChapter(){
      chapterFactory.getChapter()
        .then(function(chapter){
          populateChapter(chapter);
          $ionicSlideBoxDelegate.update();
        })
        .finally(function(){
          loading.hideLoading();
        });
    }

    function getChapterId(){
      vm.chapterId = chapterFactory.getChapterId();
    }

    function populateChapter(pageList){
      angular.forEach(pageList, function(page, key){
        vm.pages.push({'imgURL': page.image.replace('small', 'final')});
      });
    }

    function setCurrentLanguage(){
      vm.currentLanguage = languageDictionary[$localstorage.get('language', 'en')];
    }

    function setLastChapterRead(){
      $localstorage.set('lastChapterRead', chapterFactory.getChapterId());
    }

    function hasReadBefore(){
      return $localstorage.get('lastChapterRead', false);
    }

    function setZoom(page) {
      var scrollHandle = getScrollHandle(page);
      if(!userHasBeenZoom){
        scrollHandle.zoomTo(2, true);
        $ionicSlideBoxDelegate.enableSlide(false);
      }else{
        scrollHandle.zoomTo(1, true);
        $ionicSlideBoxDelegate.enableSlide(true);
      }
      userHasBeenZoom = !userHasBeenZoom;
    }

    function showAdmobInterstital(){
      if(window.device && typeof AdMob !== 'undefined')
        AdMob.showInterstitial();
    }

    function setTutorialImages(){
      vm.tutorialImages = [
        {
        	'src' : 'img/tutorial-slide.jpg',
      	},
        // {
        //   'src' : '../../img/tutorial-tap.jpg',
        // }
      ];
    }

    function showModal(){
      if(!hasReadBefore()){
        createModal();
      }
    }

    function createModal(){
      vm.modal = $ionicModal.fromTemplate(
        '<div class="modal image-modal tutorial transparent" ng-click="chapter.closeModal()">' +
          '<ion-slide-box on-slide-changed="chapter.slideChanged(index)" show-pager="false">' +
            '<ion-slide ng-repeat="tutorialImage in chapter.tutorialImages">' +
              '<img src="{{tutorialImage.src}}" class="tutorial-image" />' +
            '</ion-slide>' +
          '</ion-slide-box>' +
        '</div>',
      {
        scope: $scope,
        animation: 'slide-in-up'
      });
      openModal();
    }

    function openModal() {
      $ionicSlideBoxDelegate.slide(0);
      vm.modal.show();
      console.log(vm.tutorialImages);
    }

    vm.closeModal = function() {
      vm.modal.hide();
      showAdmobInterstital();
    };
  }
})();
