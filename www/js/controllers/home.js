(function(){
  'use strict';

  angular
    .module('test')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['languageDictionary', '$localstorage', '$ionicPopup', '$location', 'loading', '$timeout', '$ionicSideMenuDelegate'];

  function HomeCtrl(languageDictionary, $localstorage, $ionicPopup, $location, loading, $timeout, $ionicSideMenuDelegate) {
    var vm = this;

    activate();

    function activate(){
      setCurrentLanguage();
      waitToDecideViewChange();
    }

    function getChapterNumber(){
      return $localstorage.get('lastChapterRead', false);
    }

    function goToLastReadChapter(){
      $ionicPopup.confirm({
        title: vm.currentLanguage.goToChapterTitle,
        template: vm.currentLanguage.goToChapterText,
        cancelText: vm.currentLanguage.cancelText,
        okText: vm.currentLanguage.acceptText,
      }).then(function(hasConfirmed) {
        if(hasConfirmed)
          $location.url('app/chapters/' + getChapterNumber());
        else
          openSideBar();
      });
    }

    function goToChooseLanguage(){
      $location.url('app/language');
    }

    function goToChapterList(){
      $ionicSideMenuDelegate.toggleLeft(true);
    }

    function hasChosenLanguage(){
      return $localstorage.get('chooseLanguage', false);
    }

    function hasReadBefore(){
      return $localstorage.get('lastChapterRead', false);
    }

    function openSideBar(){
      angular.element('ion-header-bar .buttons-left button').click();
    }

    function setCurrentLanguage() {
      vm.currentLanguage = languageDictionary[$localstorage.get('language', 'en')];
    }

    function setStartUrl(){
      if(hasReadBefore())
        goToLastReadChapter();
      /*else if(!hasChosenLanguage())
        goToChooseLanguage();*/
      else if(hasChosenLanguage())
        goToChapterList();
    }

    function waitToDecideViewChange(){
      $timeout(function(){
        setStartUrl();
      }, 1500);
    }
  }
})();
