(function(){
  'use strict';

  angular
    .module('test')
    .controller('MenuCtrl', MenuCtrl);

  MenuCtrl.$inject = ['$scope', '$window', 'languageDictionary', '$localstorage', '$ionicPopup', '$location', 'chaptersFactory', '$timeout'];

  function MenuCtrl($scope, $window, languageDictionary, $localstorage, $ionicPopup, $location, chaptersFactory, $timeout) {
    var vm = this;
    vm.chapters = [];
    vm.languages = [];
    vm.reloadContent = reloadContent;
    vm.setAsCurrentLanguage = setAsCurrentLanguage;
    vm.setCurrentLanguage = setCurrentLanguage;
    vm.showLanguagesPopup = showLanguagesPopup;

    activate();

    function activate(){
      setCurrentLanguage();
      getChapters();
      populateLanguages();
      waitToDecideViewChange();
    }

    function getChapters(){
      chaptersFactory.getChapters()
        .then(function(chapters){
          populateChapters(chapters);
        });
    }

    function waitToDecideViewChange(){
      console.log(hasChosenLanguage());
      if(!hasChosenLanguage()){
        console.log('entra');
        $timeout(function(){
          console.log('timeout');
          showLanguagesPopup();
        }, 3000);
      }
    }

    function hasChosenLanguage(){
      return $localstorage.get('chooseLanguage', false);
    }

    // Languages
    function openSideBar(){
      angular.element('ion-header-bar .buttons-left button').click();
    }

    function populateChapters(chapters){
      angular.forEach(chapters, function(chapter, key){
        var chapterObj = [];
        chapterObj.title = chapter.value;
        chapterObj.coverImage = chapter.image;
        chapterObj.id = key + 1;
        vm.chapters.push(chapterObj);
      });
    }

    // Languages
    function populateLanguages(){
      angular.forEach(languageDictionary, function(language){
        var languageObj = [];
        languageObj.imgURL = language.imgURL;
        languageObj.selectLanguage = language.selectLanguage;
        languageObj.languagesText = language.languagesText;
        languageObj.languageCode = language.languageCode;
        languageObj.menuText = language.menuText;
        vm.languages.push(languageObj);
      });
    }

    function reloadContent(){
      $window.location.reload();
    }

    // Languages
    function setAsCurrentLanguage(language){
      $localstorage.set('language', language.languageCode);
      vm.currentLanguage = languageDictionary[$localstorage.get('language', 'en')];
      $localstorage.set('chooseLanguage', true);
      vm.languagePopup.close();
      openSideBar();
    }

    function setCurrentLanguage(){
      vm.currentLanguage = languageDictionary[$localstorage.get('language', 'en')];
    }

    // Languages
    function setChooseLanguage(){
      vm.chooseLanguage = $localstorage.get('chooseLanguage', false);
    }

    function showLanguagesPopup(){
      console.log('popup');
      vm.languagePopup = $ionicPopup.show({
        title: vm.currentLanguage.selectLanguage,
        template: '<img class="language-image" ng-src={{language.imgURL}} ng-repeat="language in menu.languages" ng-click="menu.setAsCurrentLanguage(language)" />',
        scope: $scope,
      });
    }
  }
})();
