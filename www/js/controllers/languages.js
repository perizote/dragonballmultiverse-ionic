(function(){
  'use strict';

  angular
    .module('test')
    .controller('LanguagesCtrl', LanguagesCtrl);

  LanguagesCtrl.$inject = ['languageDictionary', '$localstorage', 'loading', '$ionicSideMenuDelegate'];

  function LanguagesCtrl(languageDictionary, $localstorage, loading, $ionicSideMenuDelegate) {
    var vm = this;
    vm.languages = [];
    vm.setAsCurrentLanguage = setAsCurrentLanguage;

    activate();

    function activate(){
      setCurrentLanguage();
      setChooseLanguage();
      populateLanguages();
    }

    function openSideBar(){
      angular.element('ion-header-bar .buttons-left button').click();
    }

    function populateLanguages(){
      angular.forEach(languageDictionary, function(language, key){
        var languageObj = [];
        languageObj.imgURL = language.imgURL;
        languageObj.selectLanguage = language.selectLanguage;
        languageObj.languagesText = language.languagesText;
        languageObj.languageCode = language.languageCode;
        languageObj.menuText = language.menuText;
        vm.languages.push(languageObj);
      });
    }

    function setAsCurrentLanguage(language){
      $localstorage.set('language', language.languageCode);
      vm.currentLanguage = languageDictionary[$localstorage.get('language', 'en')];
      $localstorage.set('chooseLanguage', true);
      openSideBar();
    }

    function setCurrentLanguage(){
      vm.currentLanguage = languageDictionary[$localstorage.get('language', 'en')];
    }

    function setChooseLanguage(){
      vm.chooseLanguage = $localstorage.get('chooseLanguage', false);
    }
  }
})();
