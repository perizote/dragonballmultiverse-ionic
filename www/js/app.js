(function(){
  'use strict';

  angular
    .module('test', ['ngCordova', 'ionic'])
    .constant('API_KEY', 'f248b254-cdb2-4ab7-b6a9-c3d4e33fb4f0%3AR1w%2BNqcgaRA8WUaQFFgz3YNgvKJm%2BGiV%2BdrUd5JdEs9nfCgdDB%2BmLrSo%2F7q5lpLBoO87D6C98f6EmkYIuiIqCg%3D%3D')
    .provider('chapters_url', function(){
      return{
        $get: function(){
          return{
            pre_lang: 'https://api.import.io/store/data/41304089-7b80-4183-aeca-4bc7108a2cf5/_query?input/webpage/url=http%3A%2F%2Fwww.dragonball-multiverse.com%2F',
            post_lang: '%2Fchapters.html&_user=f248b254-cdb2-4ab7-b6a9-c3d4e33fb4f0&_apikey='
          };
        }
      };
    })
    .provider('chapter_url', function(){
      return{
        $get: function(){
          return{
            pre_lang: 'https://api.import.io/store/data/273bce5a-c346-473e-a0b2-8370c7be5bc8/_query?input/webpage/url=http%3A%2F%2Fwww.dragonball-multiverse.com%2F',
            post_lang: '&_user=f248b254-cdb2-4ab7-b6a9-c3d4e33fb4f0&_apikey=',
            chapter_query: '%2Fchapters.html%3Fchapter%3D'
          };
        }
      };
    })
    .service('loading', function($ionicLoading){
      return{
        showLoading: function() {
          $ionicLoading.show({
            template: '<ion-spinner icon="android" class="spinner-assertive"></ion-spinner>'
          });
        },
        hideLoading: function(){
          $ionicLoading.hide();
        }
      };
    })
    .run(function($ionicPlatform, $timeout, $ionicPopup) {
      $ionicPlatform.ready(function() {

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }
        if(window.Connection) {
          if(navigator.connection.type == Connection.NONE) {
            $timeout(function(){
              navigator.app.exitApp();
            }, 1500);
            $ionicPopup.show({
              title: 'ERROR',
              template: 'NO INTERNET CONNECTION DETECTED',
            });

          }
        }
        if (window.device && typeof AdMob !== 'undefined') {
          var admob_key = {
            banner: 'ca-app-pub-9921730188063588/6096846755',
            interstitial: 'ca-app-pub-9921730188063588/1527046357',
          };

          AdMob.createBanner({
            adId: admob_key.banner,
            position: AdMob.AD_POSITION.BOTTOM_CENTER,
            isTesting: false,
            adSize: 'SMART_BANNER',
            success: function(){
            },
            error: function(){
              console.log('failed to create banner');
            }
          });

          AdMob.prepareInterstitial( {adId:admob_key.interstitial, autoShow:false} );
        }
      });
    })

    .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
      // Forces native scroll to Android device (great improve of performance)
      if (!ionic.Platform.isIOS()) {
        $ionicConfigProvider.scrolling.jsScrolling(false);
      }

      $stateProvider
        .state('app', {
          cache: false,
          url: "/app",
          abstract: true,
          templateUrl: "templates/menu.html",
          controller: 'MenuCtrl as menu'
        })
        .state('app.language', {
          cache: false,
          url: "/language",
          views: {
            'menuContent': {
              templateUrl: "templates/language.html",
              controller: "LanguagesCtrl as languages"
            }
          }
        })
        .state('app.single', {
          cache: false,
          url: "/chapters/:chapterId",
          views: {
            'menuContent': {
              templateUrl: "templates/chapter.html",
              controller: 'ChapterCtrl as chapter'
            }
          }
        })
        .state('app.home', {
          cache: false,
          url: "/home",
          views: {
            'menuContent': {
              templateUrl: "templates/home.html",
              controller: 'HomeCtrl as home'
            }
          }
        });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/app/home');
    });
})();
