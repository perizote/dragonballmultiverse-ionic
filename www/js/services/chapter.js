(function(){
  'use strict';

  angular
    .module('test')
    .factory('chapterFactory', chapterFactory);

  chapterFactory.$inject = ['$http', 'API_KEY', 'chapter_url', 'languageDictionary', '$localstorage', '$stateParams'];

  function chapterFactory($http, API_KEY, chapter_url, languageDictionary, $localstorage, $stateParams){

    return{
      getChapter: getChapter,
      getChapterId: getChapterId
    };

    function currentLanguage(){
      return languageDictionary[$localstorage.get('language', 'en')];
    }

    function getChapter(){
      return $http.get(chapter_url.pre_lang + currentLanguage().languageCode +
        chapter_url.chapter_query + getChapterId() + chapter_url.post_lang + API_KEY)
        .then(getChapterComplete);
    }

    function getChapterId(){
      return $stateParams.chapterId;
    }

    function getChapterComplete(chapter){
      return chapter.data.results;
    }
  }
})();
