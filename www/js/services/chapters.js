(function(){
  'use strict';

  angular
    .module('test')
    .factory('chaptersFactory', chaptersFactory);

  chaptersFactory.$inject = ['$http', 'API_KEY', 'chapters_url', 'languageDictionary', '$localstorage'];

  function chaptersFactory($http, API_KEY, chapters_url, languageDictionary, $localstorage){

    return{
      getChapters: getChapters
    };

    function currentLanguage(){
      return languageDictionary[$localstorage.get('language', 'en')];
    }

    function getChapters(){
      return $http.get(chapters_url.pre_lang + currentLanguage().languageCode + chapters_url.post_lang + API_KEY)
        .then(getChaptersComplete);
    }

    function getChaptersComplete(chapters){
      return chapters.data.results;
    }
  }
})();
